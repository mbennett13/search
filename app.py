import os
import search
from flask import Flask
from flask import Response
from flask import request
import json

app = Flask(__name__)

@app.route('/')
def hello_world():
    output = {
    "results" : "",
    "pageid0" : 0,
    "uri" : "",
    "indexedat" : "",
    "content" : ""
    };
    x = request.args.get('x')
    results = search.search(x)
    output.update(pageid0 = results[0])
    output.update(uri = results[1])
    output.update(indexedat = results[2])
    output.update(content = results[3])

    rep = Response(json.dumps(output))
    rep.headers["Content-Type"] = "application/json"
    rep.headers["Access-Control-Allow-Origin"] = "*"
    return rep

if __name__ == "__main__":
    app.run(host='0.0.0.0')
